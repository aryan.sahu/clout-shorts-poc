export function formatNumberInCommas(num: number | string) {
  if (!num) return num;

  return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

export const setBodyScrollBehavior = (type: 'hidden' | 'default') => {
  if (typeof window === 'undefined') return;

  switch (type) {
    case 'hidden':
      document.body.classList.add('overflow-hidden', 'overscroll-contain');
      break;
    case 'default':
      document.body.classList.remove('overflow-hidden', 'overscroll-contain');
      break;
    default:
      break;
  }
};