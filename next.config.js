/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  images: {
    domains: [
      'o1product-images.cdn.myownshop.in',
    ]
  }
}

module.exports = nextConfig
