import { useEffect, useState } from "react";
import SwipeableViews from "react-swipeable-views";

export const mockVideos = [
  "https://o1-video.cdn.myownshop.in/64dc9a301e236dd0abb33d5f43e17f48.mp4",
  "https://o1-video.cdn.myownshop.in/e6a8653e56a76f9fb7cf57bfc275bf34.mp4",
  "https://o1-video.cdn.myownshop.in/9f63cfb85324ac7b9393f98f73004b7d.mp4",
  "https://o1-video.cdn.myownshop.in/e93ea8df73b343d7cd9d097c1923a4f1.mp4",
  "https://o1-video.cdn.myownshop.in/95004aecbf7b410cdbdcaa6cf6df1eb7.mp4",
  "https://o1-video.cdn.myownshop.in/a2af7b7c5722e9530367e6e48fe5438f.mp4",
  "https://o1-video.cdn.myownshop.in/4f4e6c928604bdc2efebcbbdfda3310c.mp4",
  "https://o1-video.cdn.myownshop.in/603cab653c4b1223a00ddec7d31b64f4.mp4",
  "https://o1-video.cdn.myownshop.in/04af86ac6d057e0f7c77ecbcd42ce760.mp4",
  "https://o1-video.cdn.myownshop.in/7f3ff2cd5bff613ae2b797bfec9c8f04.mp4",
  "https://o1-video.cdn.myownshop.in/9960547210630aa19dd4d7bdc41ec8da.mp4",
  "https://o1-video.cdn.myownshop.in/d8c7e4c6fe7818b0ab068cfea15e60f9.mp4",
  "https://o1-video.cdn.myownshop.in/138da07b7918d2b329e1e89674d43c61.mp4",
  "https://o1-video.cdn.myownshop.in/ab47cb2c177ef84ac3ecb9f54644f7ac.mp4",
  "https://o1-video.cdn.myownshop.in/597e52cfacd7c97d850b8cc8b2a2b180.mp4",
  "https://o1-video.cdn.myownshop.in/4fea2eb6958537c1fb283d220699f8a8.mp4",
];

const styles = {
  slideContainer: {
    height: "100vh",
    maxWidth: "473px",
    margin: "auto",
    alignItems: "center",
    justifyContent: "flex-start",
  },
  slide: {
    padding: 15,
    minHeight: "100vh",
    color: '#fff',
  },
};

const Shorts = () => {
  const videos = mockVideos;

  const handleChangeIndex = (index: number, indexLatest: number) => {
    console.log({ index, indexLatest })
    // @ts-ignore
    document.getElementById(`myvideo-${indexLatest}`)?.pause();
    // @ts-ignore
    document.getElementById(`myvideo-${index}`)?.play();
  }

  return (
    <div className="bg-[black] text-[#ffffff]">
      <SwipeableViews 
        containerStyle={styles.slideContainer}
        axis="y" 
        resistance
        onChangeIndex={handleChangeIndex}
        index={0}
      >
        {videos?.map((video: string, index) => (
          <video
            key={index}
            controls={index === 0}
            id={`myvideo-${index}`}
            className="h-[100vh] object-contain"
            playsInline
            loop
          >
            <source
              src={video}
              type="video/mp4"
            />
          </video>
        ))}
      </SwipeableViews>
    </div>
  );
}

export default Shorts;
