import Image from "next/image";
import Link from "next/link";
import closeIcon from "../../../assets/close-icon-white.svg"
import { memo } from "react";

interface VideoHeaderControllerProps {
  headerIconUrl?: string;
  headerName?: string;
  redirectUrl?: string;
  onClose: () => void;
}

const VideoHeaderController = ({ headerIconUrl, headerName, redirectUrl, onClose }: VideoHeaderControllerProps) => {
  return (
    <div className="video-header-controller absolute left-0 right-0 top-0 z-[150] p-4">
      <div className="flex justify-between items-center">
        {headerName && (
          <Link 
            passHref 
            href={redirectUrl || "#"}
          >
            <a className="text-sm font-bold w-fit flex gap-[8px] items-center">
              {headerIconUrl && (
                <div className="w-[32px] h-[32px] border rounded-full overflow-hidden">
                  <Image
                    src="/"
                    width={32}
                    height={32}
                    loading="eager"
                  />
                </div>
              )}
              <span className="border-b">{headerName}</span>
            </a>
          </Link>
        )}
        <button 
          className="flex items-center justify-center w-[20px] h-[20px] ml-auto"
          onClick={onClose}
        >
          <Image
            src={closeIcon}
            height={20}
            width={20}
            loading="eager"
          />
        </button>
      </div>
    </div>
  );
}

const areEqual = (prevProps: VideoHeaderControllerProps, nextProps: VideoHeaderControllerProps) => {
  return (
    prevProps.headerIconUrl === nextProps.headerIconUrl &&
    prevProps.headerName === nextProps.headerName &&
    prevProps.redirectUrl === nextProps.redirectUrl
  );
}

export default memo(VideoHeaderController, areEqual);
