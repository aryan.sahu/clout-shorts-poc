import dynamic from "next/dynamic";

const FullScreenVideoModal = dynamic(() => import("./innerComponents/FullScreenVideoModal"))
const PortraitVideo = dynamic(() => import("./innerComponents/PortraitVideo"))

const PortraitProductVideoPlayer = () => {
  return (
    <FullScreenVideoModal>
     <PortraitVideo headerName="New Arrivals" products={[]} />
    </FullScreenVideoModal>
  );
}

export default PortraitProductVideoPlayer
