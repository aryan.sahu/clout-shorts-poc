interface FullScreenVideoModalProps {
  children: React.ReactElement;
}

const FullScreenVideoModal = ({ children }: FullScreenVideoModalProps) => {
  return (
    <div className="fixed flex top-0 bottom-0 right-0 left-0 z-[130]">
      {children}
    </div>
  )
}

export default FullScreenVideoModal;
