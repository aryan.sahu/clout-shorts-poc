import { useEffect, useMemo, useState } from "react";
import dynamic from "next/dynamic";
import SwipeableViews from "react-swipeable-views";
import { setBodyScrollBehavior } from "../../../utils/misc";
import ReactPlayer from "react-player/lazy";
import { mockVideos } from "../..";


const HeaderController = dynamic(() => import("./HeaderController"));
const VideoProductInfo = dynamic(() => import("./VideoProductInfo"));

interface PortraitVideoProps {
  products: [];
  onComponentDidMount?: () => void;
  onVideoChange?: (currentSlide: number, prevSlide: number) => void;
  headerName?: string;
  headerIcon?: string;
}

const PortraitVideo = ({ products, onComponentDidMount, onVideoChange, headerName, headerIcon }: PortraitVideoProps) => {
  // const BOTTOM_NAV_HEIGHT = 48;

  const [viewportHeight, setViewportHeight] = useState('100vh');
  const [currentSlide, setCurrentSlide] = useState(0);

  useEffect(() => {
    onComponentDidMount && onComponentDidMount();
    // handleBottomNav('hide');

    if (typeof window !== 'undefined') {
      setViewportHeight(`${window.innerHeight}px`);
      setBodyScrollBehavior('hidden');
    }

    return () => handleOnClose();
  }, []);

  // Hide/Visible bottom nav
  // const handleBottomNav = (action: 'hide' | 'visible') => {
  //   switch (action) {
  //     case 'hide':
  //       if (bottomNav?.isVisible) {
  //         setViewportHeight(`${parseFloat(viewportHeight)+BOTTOM_NAV_HEIGHT}px`);
  //         dispatch(setBottomNav(false));
  //       }
  //       break;
  //     case 'visible':
  //       if (!bottomNav.isVisible) {
  //         setViewportHeight(`${parseFloat(viewportHeight)-BOTTOM_NAV_HEIGHT}px`);
  //         dispatch(setBottomNav(true));
  //       }
  //       break;
  //     default:
  //       break;
  //   }
  // }

  // Styles
  const swipeAbleViewStyles = useMemo(() => ({
    slideContainer: {
      height: viewportHeight,
      maxWidth: "473px",
      margin: "auto",
      alignItems: "center",
      justifyContent: "flex-start",
    },
  }), [viewportHeight]);

  // Close video player
  const handleOnClose = () => {
    setBodyScrollBehavior('default');
    // dispatch(setPortraitVideoPlayer(false));
    // handleBottomNav('visible');
  }

  const handleVideoChange = (currentSlide: number, prevSlide: number) => {
    setCurrentSlide(currentSlide);
    onVideoChange && onVideoChange(currentSlide, prevSlide);
  }

  const handleOnplay = (currentSlideIndex: number) => {
    if (currentSlideIndex === currentSlide) {
      // handleBottomNav('hide');
    }
  }

  const handleOnPause = (currentSlideIndex: number) => {
    if (currentSlideIndex === currentSlide) {
      // handleBottomNav('visible');
    }
  }

  const handleOnVideoEnded = (currentSlideIndex: number) => {
    // check if current slide index + 1 is less than products length
    if (currentSlideIndex + 1 < mockVideos.length) {
      const nextSlideIndex = currentSlideIndex + 1;

      handleVideoChange(nextSlideIndex, currentSlideIndex);
      setCurrentSlide(nextSlideIndex);
    } else { // else reset to first slide
      handleVideoChange(0, currentSlideIndex);
      setCurrentSlide(0);
    }
  }

  return (
    <div className="flex justify-center w-full h-full bg-[black] text-[white]">
      <div className="relative justify-center flex h-full max-w-[496px] min-w-full">
        {/* Video Slider */}
        <SwipeableViews
          containerStyle={swipeAbleViewStyles.slideContainer}
          axis="y" 
          resistance
          onChangeIndex={handleVideoChange}
          slideStyle={{ height: viewportHeight }}
          index={currentSlide}
        >
          {mockVideos.map((url, index) => (
            <div
              key={index}
              className={`portrait-video-container relative w-full h-full`}
            >
              <HeaderController
                onClose={handleOnClose}
                headerIconUrl={headerIcon}
                headerName={headerName}
              />
              {/* ----- */}
              <ReactPlayer
                url={url}
                height='100%'
                width='100%'
                controls
                playing={currentSlide === index}
                onPlay={() => handleOnplay(index)}
                onPause={() => handleOnPause(index)}
                onEnded={() => handleOnVideoEnded(index)}
              />
              {/* ----- */}
              <VideoProductInfo
                couponDiscountedPrice={2332}
                productOriginalPrice={3454}
                productImageUrl={""}
                productName={"Dummy Product Name"}
                productData={[]}
                isProductDetailsDrawerVisible={false}
                // handleBottomNav={handleBottomNav}
              />
            </div>
          ))}
        </SwipeableViews>
      </div>
    </div>
  );
};

export default PortraitVideo;
