
import { useEffect, useState } from "react";

const mockVideos = [
  // "https://download-video.akamaized.net/2/playback/34a49c16-56fe-4844-8293-9b699d0d00ef/47f15a3a-9a6c3ab9?__token__=st=1685080678~exp=1685095078~acl=%2F2%2Fplayback%2F34a49c16-56fe-4844-8293-9b699d0d00ef%2F47f15a3a-9a6c3ab9%2A~hmac=21f76a06c42cabe418fac86083e3def7e0bfb8563835fb9a4f1f800cb34b993c&r=dXMtd2VzdDE%3D",
  // "https://download-video.akamaized.net/2/playback/c7220453-697b-4b8e-bf4c-f3ebac60a961/f39df901-4a169130?__token__=st=1685080699~exp=1685095099~acl=%2F2%2Fplayback%2Fc7220453-697b-4b8e-bf4c-f3ebac60a961%2Ff39df901-4a169130%2A~hmac=29085cf63ea0ada83e5138566c14e28d0cc286a82ffc2824938a7fb7a8db8417&r=dXMtY2VudHJhbDE%3D",
  // "https://download-video.akamaized.net/2/playback/c90f6972-bef7-4c3e-b79a-038bac8a385f/c40b8828-1af08854?__token__=st=1685080725~exp=1685095125~acl=%2F2%2Fplayback%2Fc90f6972-bef7-4c3e-b79a-038bac8a385f%2Fc40b8828-1af08854%2A~hmac=f41d987c64c7ed2c15552ad2645aaf07fa589b92ecd8f203e1bee31245d93f58&r=dXMtZWFzdDE%3D",
  "https://player.vimeo.com/progressive_redirect/playback/829789164/rendition/360p/file.mp4?loc=external&oauth2_token_id=1617621117&signature=ff124d93a88f2573b7129c1bd4a462b95d4ef72b09fd24d23740402bc2b171e6",
  "https://player.vimeo.com/progressive_redirect/playback/829788526/rendition/360p/file.mp4?loc=external&oauth2_token_id=1617621117&signature=d17e4b8fac7435264b311586331ed83909acc6041cdf1920d49f4e10f313c3d7",
  // "https://download-video.akamaized.net/2/playback/818b02d3-187c-44cb-922b-1c966f80fab2/b968e553-cf3302ba?__token__=st=1685080807~exp=1685095207~acl=%2F2%2Fplayback%2F818b02d3-187c-44cb-922b-1c966f80fab2%2Fb968e553-cf3302ba%2A~hmac=f4b32410649359e78bebd7e5843bfe9a540487312e2f255ddea419de5659b118&r=dXMtZWFzdDE%3D",
  "https://player.vimeo.com/progressive_redirect/playback/829768945/rendition/360p/file.mp4?loc=external&oauth2_token_id=1617621117&signature=5d9326a062c8e6d46175f661921d24198dfa91262f12c577a21656c47c4517ba"
];

const Shorts = () => {
  const videos = mockVideos;
  // const [showPlayButton, setShowPlayButton] = useState(true);

  useEffect(() => {
    if (typeof window !== undefined) {
      let options = {
        root: null,
        rootMargin: '0px',
        threshold: 0.8
      };

      let observer = new IntersectionObserver(observeViewElementRef, options);
      const videos = document.querySelectorAll("video");

      videos.forEach(vide => {
        observer.observe(vide);
      });
    }
  }, []);

  const observeViewElementRef = (entries: any) => {
    entries.forEach((entry: any) => {
      const video = entry.target;
      const isPlaying = video.currentTime > 0 && !video.paused && !video.ended && video.readyState > video.HAVE_CURRENT_DATA;

      if (entry.target.id?.startsWith('myvideo')) {
          if (entry.isIntersecting && !isPlaying) {
            entry.target.play();
          }
          else {
              entry.target.pause();
          }
      }
    });
  }

  // const handleClick = () => {
  //   const video = document.getElementById("myvideo-0");

  //   // @ts-ignore
  //   if (video?.paused) {
  //     setShowPlayButton(false)
  //     // @ts-ignore
  //     video?.play()
  //   }
  //   else {
  //     setShowPlayButton(false);
  //   }
  // }

  return (
    <div className="flex flex-col bg-[black] text-[#ffffff]">
      {videos?.map((video: string, index) => (
        <div
          key={index}
          id={`myvideo-wrapper-${index}`}
          className="relative flex flex-col h-[100vh] max-w-[473px] m-[auto] z-10"
        >
          {/* {showPlayButton && (
            <button 
              className="absolute w-[50px] h-[50px] bg-[rgb(0,0,0,0.47)] rounded-full p-2 cursor-pointer z-20 top-[48%] left-[48%] text-sm"
              onClick={handleClick}
            >
              Play
            </button>
          )} */}
          <video 
            controls
            id={`myvideo-${index}`}
            className="h-[100vh]"
            playsInline
            // onClick={handleClick}
          >
            <source
              src={video}
              type="video/webm"
            />
          </video>
        </div>
      ))}
    </div>
  );
}

export default Shorts;
