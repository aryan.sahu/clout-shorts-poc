import Image from "next/image";
import { memo, useCallback, useEffect } from "react";
import { formatNumberInCommas } from "../../../utils/misc";

interface VideoProductInfoProps {
  couponDiscountedPrice: number;
  productOriginalPrice: number;
  productImageUrl: string;
  productName: string;
  productData: [];
  onProductCardClick?: () => void;
  isProductDetailsDrawerVisible?: boolean;
  handleBottomNav?: (action: 'hide' | 'visible') => void;
}

const VideoProductInfo = ({
  couponDiscountedPrice,
  productOriginalPrice,
  productImageUrl,
  productName,
  onProductCardClick,
  isProductDetailsDrawerVisible,
  handleBottomNav,
}: VideoProductInfoProps) => {
  

  const memoizedFormatNumberInCommas = useCallback(formatNumberInCommas, [
    couponDiscountedPrice,
    productOriginalPrice,
  ]);

  useEffect(() => {
    handleBottomNav && handleBottomNav(isProductDetailsDrawerVisible ? "visible" : "hide");
  }, [isProductDetailsDrawerVisible]);

  const handleProductCardClick = () => {
    onProductCardClick && onProductCardClick();
  };

  return (
    <div className="video-product-info absolute text-[white] bottom-[56px] left-0 right-0 z-[150] p-4 text-sm">
      <div className="flex items-end gap-[20px] justify-end">
        {/* product Name */}
        <span className="max-w-[300px] text-overflow text-ellipsis overflow-hidden line-clamp-3 mb-[10px]">
          {productName}
        </span>

        {/* Product Card */}
        <div
          className="relative min-w-[102px] min-h-[172px] p-[5px] flex flex-col gap-[5px] border border-[rgb(255,255,255,0.4)] rounded ml-auto"
          style={{
            background:
              "linear-gradient(to bottom, rgb(255 255 255 / 0%), rgb(255 255 255 / 40%), rgb(255 255 255 / 30%))",
          }}
          onClick={handleProductCardClick}
        >
          {/* Product Image */}
          {productImageUrl && (
            <div className="w-[90px] h-[110px] bg-[#EDEDED] rounded overflow-hidden">
              <Image
                src={productImageUrl}
                width={90}
                height={110}
                objectFit="cover"
                alt="productImage"
                loading="eager"
              />
            </div>
          )}

          {/* Product Price */}
          <div className="flex items-baseline gap-[5px] mt-auto">
            <span className={"text-xs font-semibold"}>
              ₹{memoizedFormatNumberInCommas(couponDiscountedPrice)}
            </span>
            {couponDiscountedPrice !== productOriginalPrice && (
              <span
                className={"text-[9px] font-medium leading-[10px] line-through"}
              >
                ₹{memoizedFormatNumberInCommas(productOriginalPrice)}
              </span>
            )}
          </div>

          {/* View Button */}
          <button className="bg-[black] rounded w-full h-[26px] justify-center item-center text-[10px] leading-[12px] font-bold">
            VIEW
          </button>
        </div>
      </div>
    </div>
  );
};

const areEqual = (
  prevProps: VideoProductInfoProps,
  nextProps: VideoProductInfoProps
) => {
  return (
    prevProps.couponDiscountedPrice === nextProps.couponDiscountedPrice &&
    prevProps.productOriginalPrice === nextProps.productOriginalPrice &&
    prevProps.productImageUrl === nextProps.productImageUrl &&
    prevProps.productName === nextProps.productName &&
    prevProps.isProductDetailsDrawerVisible === nextProps.isProductDetailsDrawerVisible
  );
};

export default memo(VideoProductInfo, areEqual);
